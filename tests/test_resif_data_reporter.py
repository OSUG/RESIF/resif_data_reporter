import pytest
from random import getrandbits, choices
import string
import subprocess
import resifdatareporter
import yaml
from pprint import pprint


with open('config.yml.example', 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.SafeLoader)

document="""
volumes:
  - path: /path/1
    type: validated
  - path: /data/all/example
    type: bud
"""

@pytest.fixture(scope='session')
def sds(tmpdir_factory):
  for t in ['bud','validated']:
    d = tmpdir_factory.mktemp("%s"%(t), numbered=False)
    for y in range(2004,2018):
      d = tmpdir_factory.mktemp("%s/%d"%(t,y), numbered=False)
      for i in ['G', 'FR', 'MT']:
        d = tmpdir_factory.mktemp("%s/%d/%s"%(t, y, i), numbered=False)
        station = ''.join(choices(string.ascii_uppercase + string.digits, k=4))
        d = tmpdir_factory.mktemp("%s/%d/%s/%s"%(t, y, i,station), numbered=False)
        size = getrandbits(12)
        subprocess.call(['dd', 'if=/dev/zero', "of=%s/%s/%d/%s/%s/file0"%(tmpdir_factory.getbasetemp(),t, y, i, station),"count=%d"%(size)])
        pprint("Creating file %s/%s/%d/%s/%s/file0"%(tmpdir_factory.getbasetemp(),t, y, i, station))
  return tmpdir_factory.getbasetemp()


def test_scan_volume(sds):
  print(sds)
  rd = resifdatareporter.scan_volume(sds+"/bud")
  pprint(rd)
  assert len(rd) > 0
  for i in rd:
      pprint(i)
      assert int(i['size']) > 0
  # on pourrait faire des asserts plus compliqués mais bon ...

def test_scan_volumes_well_formed_parameter(sds):
    volumes_desc = """
volumes:
  - path: %s/validated
    type: validated
  - path: %s/bud
    type: bud
"""%(sds,sds)
    volumes = yaml.load(volumes_desc, Loader=yaml.CLoader)
    data = resifdatareporter.scan_volumes(volumes['volumes'])
    assert len(data) > 2
    assert 'type' in data[0]
