FROM python:3.12-slim as build
COPY requirements.txt /
RUN apt-get update && apt-get install -y --no-install-recommends libpq-dev  build-essential
RUN pip install setuptools wheel
RUN pip install --no-cache-dir -r /requirements.txt
WORKDIR /usr/src/resifdatareporter

FROM python:3.12-slim
COPY --from=build /usr/local/lib/python3.12/site-packages /usr/local/lib/python3.12/site-packages
COPY resifdatareporter/resifdatareporter.py resifdatareporter/
COPY setup.py .
COPY README.md .
RUN pip install -e .
USER 1000
CMD resifdatareporter --config-file /config/config.yaml
