#!/bin/env python
import logging
import os
import sys
import io
import subprocess
import re
from datetime import datetime, date, timedelta
import yaml
import psycopg2
import click
from pathlib import Path

logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.INFO)
logger = logging.getLogger(__name__)

def scan_volume(path):
    """
    Scanne un volume indiqué par son chemin (path).
    La fonction lance une commande "du -d4 path" et analyse chaque ligne renvoyée.
    Elle renvoie une liste de dictionnaires :
    [ {year: 2011, network: 'G', size: '100', station: 'STAT', channel: 'BHZ.D'}, ...]
    """
    data = []
    volume = os.path.realpath(path) + "/"
    logger.debug("Volume %s", volume)
    starttime = datetime.now()
    proc = subprocess.Popen(
        ["du", "--exclude", ".snapshot", "-b", "-d4", volume], stdout=subprocess.PIPE
    )
    for l in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
        l = l.strip()
        (size, path) = l.split("\t")
        # On ne garde que le chemin qui nous intéresse
        path = path.replace(volume, "").split("/")
        # Ne pas considérer le seul chemin de niveau 1
        if len(path) == 4:
            logger.debug("path: %s, size: %s", path, size)
            try:
                (channel, _) = path[3].split(".")
            except ValueError:
                logger.info("%s is probably not a normal path. Skip it.", path)
                continue
            # Si le chemin commence par une année :
            if re.match("[1-9][0-9]{3}", path[0]):
                network = path[1]
                year = path[0]
            else:
                year = path[1]
                network = path[0]
            is_permanent = not re.match("^[0-9XYZ]", network)
            # On vérifie le format du network pour éviter les mauvaises insertions
            if re.match("^([A-W][A-Z0-9]{0,1})|([0-9XYZ][A-Z0-9][0-9]{4})$", network):
                data.append(
                    {
                        "year": year,
                        "network": network,
                        "station": path[2],
                        "channel": channel,
                        "size": size,
                        "is_permanent": is_permanent,
                    }
                )
                logger.debug(data[-1])
    logger.debug("Volume scanned in %s", datetime.now() - starttime)
    return data


def scan_ph5_volume(volpath):
    """
    Un repertoire contenant des données nodes doit être analysé différemment
    - a minima, un /du/ du répertoire et on stocke l'info seulement pour le réseau
    - sinon, en analysant les volumes ph5, mais je ne sais pas si on en a vraiment besoin.
    """
    data = []
    volume = os.path.realpath(volpath) + "/"
    logger.debug("Volume %s", volume)
    proc = subprocess.Popen(["ls", volume], stdout=subprocess.PIPE)
    for l in io.TextIOWrapper(proc.stdout, encoding="utf-8"):
        network = l.strip()
        path = f"{volume}/{network}"
        logger.debug("Scanned %s", network)
        try:
            year = int(network[2:])
        except ValueError:
            # Bon, ça n'a pas marché, on fait quoi ?
            logger.error("Unable to get year from path %s. Ignoring this one", path)
            continue
        total = 0
        for path in Path(path).rglob("*.ph5"):
            total = total + path.stat().st_size
        logger.debug(
            "Total size of %s is %s (%s GB)", network, total, total / (1024 ** 3)
        )
        # Make a statistic array with those stations dividing total size on each station.
        is_permanent = not re.match("^[1-9XYZ]", network)
        data.append(
            {
                "type": "ph5",
                "year": year,
                "network": network,
                "station": None,
                "channel": None,
                "size": total,
                "is_permanent": is_permanent,
            }
        )
    return data

def scan_validated_data(cfg):
    # Plutôt que de scanner des fichers dans le volume, on prend la valeur dans la table rall
    data = []
    logger.info("Getting sizes from inventory database.")
    with psycopg2.connect(
            dbname=cfg["pgdatabase"],
            user=cfg["pguser"],
            host=cfg["pghost"],
            port=cfg["pgport"],
            password=os.getenv('INVENTORY_PGPASSWORD', '')
    ) as conn:
        for table in cfg["repositories"]:
            logger.info("Scanning table %s", table["name"])
            with conn.cursor() as cur:
                cur.execute(f"""
                    select n.network, n.start_year, r.year, s.station, c.channel, sum(r.size)
                    from { table["name"] } as r join channel as c on r.channel_id=c.channel_id  join station as s on c.station_id = s.station_id join networks as n on n.network_id = s.network_id
                    group by 1,2,3,4,5
                """)
                for record in cur:
                    if re.match('^[A-W]', record[0]):
                        net = record[0]
                        is_perm = True
                    else:
                        net = record[0] + str(record[1])
                        is_perm = False
                    data.append(
                        {
                            "year": record[2],
                            "network": net,
                            "station": record[3],
                            "channel": record[4],
                            "size": record[5],
                            "is_permanent": is_perm,
                            "type": table["type"],
                            "volume": table["type"]
                        }
                    )
    logger.info("Done")
    return data



def scan_volumes(volumes):
    # volumes is a complex data type :
    # List of dictionaries of 2 elements (path and type)
    # [{path: /bla/bla, type: plop}, {path: /bli/bli, type: plip}]
    # En sortie, une liste de dictionnaires :
    # [ {stat}, {stat}, ]
    volume_stats = []
    starttime = datetime.now()
    for volume in volumes:
        logger.info("Preparing scan of volume %s", volume["path"])
        if "path" in volume:
            if "type" in volume and volume["type"] == "ph5":
                stats = scan_ph5_volume(volume["path"])
            else:
                stats = scan_volume(volume["path"])
                # On rajoute le type comme un élément de chaque statistique
                if "type" in volume:
                    for s in stats:
                        s["type"] = volume["type"]
            if "name" in volume:
                for s in stats:
                    s["volume"] = volume["name"]
            logger.info("Volume scanned")
            volume_stats.append(stats)
            # If a type of data was specified, then we add this tag to the stats
        else:
            raise ValueError("Volume has no path key : %s" % (volume))
    # on applati la liste de listes :
    logger.info("All volumes scanned in %s", (datetime.now() - starttime))
    return [x for vol in volume_stats for x in vol]


@click.command()
@click.option(
    "--version", flag_value=True, default=False, help="Print version and exit"
)
@click.option(
    "--config-file",
    "configfile",
    type=click.File(),
    help="Configuration file path",
    envvar="CONFIG_FILE",
    show_default=True,
    default=f"{os.path.dirname(os.path.realpath(__file__))}/config.yml",
)
@click.option(
    "--force-scan", flag_value=True, default=False, help="Force scanning of the archive"
)
@click.option(
    "--dryrun", flag_value=True, default=False, help="Do not send metrics to database"
)
@click.option("--verbose", flag_value=True, default=False, help="Verbose mode")
def cli(configfile, force_scan, dryrun, verbose, version):
    """
    Command line interface. Stands as main
    """
    if version:
        print("Version 2022.137")
        sys.exit(0)
    if verbose:
        logger.setLevel(logging.DEBUG)
    logger.info("Starting")
    try:
        cfg = yaml.load(configfile, Loader=yaml.SafeLoader)
    except yaml.YAMLError as err:
        logger.error("Could not parse %s", configfile)
        logger.error(err)
        sys.exit(1)

    # At this point we ensure that configuration is sane.
    statistics = []
    today = date.today().strftime("%Y-%m-%d")

    if not force_scan:
        # Get last stat date
        conn = psycopg2.connect(
            dbname=cfg["postgres"]["database"],
            user=cfg["postgres"]["user"],
            host=cfg["postgres"]["host"],
            port=cfg["postgres"]["port"],
        )
        cur = conn.cursor()
        cur.execute(
            "select distinct date from dataholdings order by date desc limit 1;"
        )
        last_stat_date = cur.fetchone()[0]
        logger.info("Last report: %s", last_stat_date)
        conn.close()
        if date.today() - last_stat_date > timedelta(days=(cfg["cache_ttl"])):
            logger.info("Last report is old enough. Let's get the job done.")
        else:
            logger.info(
                "Last data report made at %s. Younger than %s. Don't scan",
                last_stat_date,
                cfg["cache_ttl"],
            )
            sys.exit(0)

    statistics = scan_volumes(cfg["volumes"])
    # On évalue le volume dans l'inventaire
    statistics += scan_validated_data(cfg["inventory"])

    if dryrun:
        logger.info("Dryrun mode, dump stats and exit")
        for stat in statistics:
            print(stat)
        sys.exit(0)
        # Write to postgres database
    if "postgres" in cfg:
        logger.info("Writing %s entries to postgres database", len(statistics))
        conn = psycopg2.connect(
            dbname=cfg["postgres"]["database"],
            user=cfg["postgres"]["user"],
            host=cfg["postgres"]["host"],
            port=cfg["postgres"]["port"],
        )
        cur = conn.cursor()
        for stat in statistics:
            logger.debug(f"Insert {stat} in database")
            try:
                cur.execute(
                    """
                    INSERT INTO dataholdings (network, year, station, channel, type, size, is_permanent, volume, date)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                    ON CONFLICT (network,year,station,channel,type,date) DO UPDATE SET size = EXCLUDED.size;
                    """,
                    (
                        stat["network"],
                        stat["year"],
                        stat["station"],
                        stat["channel"],
                        stat["type"],
                        stat["size"],
                        stat["is_permanent"],
                        stat["volume"],
                        today,
                    ),
                )
            except psycopg2.Error as err:
                logger.error(err)
                logger.info(
                    cur.mogrify(
                        """
                    INSERT INTO dataholdings (network, year, station, channel, type, size, is_permanent, volume, date)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
                    ON CONFLICT (network,year,station,channel,type,date) DO UPDATE SET size = EXCLUDED.size;
                    """,
                        (
                            stat["network"],
                            stat["year"],
                            stat["station"],
                            stat["channel"],
                            stat["type"],
                            stat["size"],
                            stat["is_permanent"],
                            stat["volume"],
                            today,
                        ),
                    )
                )
        conn.commit()


if __name__ == "__main__":
    cli()
